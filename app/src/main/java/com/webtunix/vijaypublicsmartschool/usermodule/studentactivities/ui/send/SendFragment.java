package com.webtunix.vijaypublicsmartschool.usermodule.studentactivities.ui.send;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.webtunix.vijaypublicsmartschool.R;


public class SendFragment extends Fragment {



    private SendViewModel sendViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        sendViewModel =
                ViewModelProviders.of(this).get(SendViewModel.class);
        View root = inflater.inflate(R.layout.fragment_send, container, false);


        TextView textView = root.findViewById(R.id.text_send);



        textView.setText("VSS Notebook - E Learning App is a platform for school students of VIJAY PUBLIC SMART SCHOOL .The app offers comprehensive learning programs in Various subjects offered by curriculum for students .Students and their Parents can access homework very quickly. The app provides content of different subjects mentioned in the curriculum for the respective classes. It also enables pupils to join live classes taken by the school teachers from time to time. The app also provides a section named –support which enables students to clarify their queries and doubts related to curriculum content or application. To cope up with various needs of students in near future , VSS Notebook promises to inculcate various enhancements from time to time");

            return root;
    }


}
